local mgr_module = {
	"player_mgr_module",
	"net_mgr_module",
	"db_mgr_module",
}

local player_module = {
	"relation_module",
	"team_module",
	"net_module",
	"player_module",
}

return {
	mgr_module = mgr_module,
	player_module = player_module,
}